
// Теоретичні питання
// 1. Що таке події в JavaScript і для чого вони використовуються?
// Події, це дії користувача на сайті - кліки, скрол, наведення мишкою, всі взаємодіїї користувача і сайта.

// 2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
// click, doubleclick, wheel, mouseover, mouseup, mousedown, mousemove, select

// 3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?
// можна відстежувати, коли користувач використовує праву кнопку миші для визова контекстного меню, за це відповідаю подія "contextmenu".


// Практичні завдання
//  1. Додати новий абзац по кліку на кнопку:
//   По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
 const btnClick = document.querySelector('#btn-click');
 const content = document.querySelector('#content');
 btnClick.addEventListener('click', addElement);

function addElement(){
    const p = document.createElement('p');
    p.textContent = "New Paragraph";
    content.append(p)
}

//  2. Додати новий елемент форми із атрибутами:
//  Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
//   По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.

const btnInput = document.createElement('button')
btnInput.setAttribute('id', 'btn-input-create');
btnInput.textContent = 'Create element'
const content1 = document.querySelector('#content')
content1.append(btnInput)

btnInput.addEventListener('click', ()=>{
const input = document.createElement('input');
input.setAttribute('id', 'input-text');
input.setAttribute('type', 'text');
input.setAttribute('placeholder', 'Enter text');
btnInput.insertAdjacentElement('afterend', input)
})



 


